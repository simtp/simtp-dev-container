# Use Ubuntu as base image
FROM ubuntu:latest

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive



# Add the repository to Apt sources:

RUN apt update \
    && apt install -y git openjdk-11-jdk-headless openjdk-17-jdk-headless openjdk-21-jdk-headless curl wget ca-certificates ansible

RUN install -m 0755 -d /etc/apt/keyrings \
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc \
    && chmod a+r /etc/apt/keyrings/docker.asc \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt update \
    && apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose docker-compose-plugin

RUN apt clean



# Switch to the non-root user
#USER $USERNAME

# Set the working directory
#WORKDIR /home/$USERNAME

# Example: Install additional tools you might need for your development environment
# RUN curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
# RUN sudo apt-get install -y nodejs

# Command to run when the container starts
CMD ["/bin/bash"]
